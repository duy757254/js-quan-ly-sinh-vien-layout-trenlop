function informationForm(){
    var maSV=document.getElementById("txtMaSV").value
    var tenSV= document.getElementById("txtTenSV").value
    var email= document.getElementById("txtEmail").value
    var matKhau= document.getElementById("txtPass").value
    var diemToan= document.getElementById("txtDiemToan").value*1
    var diemLy= document.getElementById("txtDiemLy").value*1
    var diemHoa= document.getElementById("txtDiemHoa").value*1

    var SV=new sinhVien(maSV,tenSV,email,matKhau,diemToan,diemLy,diemHoa)
    return SV
}
// render SV lên
function renderDSSV(list){
    var contentHTML = "";
    
  for (var i = 0; i < list.length; i++) {
    var currentSV = list[i];
    var contentTr = `
        <tr>
        <td>${currentSV.ma}</td>
        <td>${currentSV.ten}</td>
        <td>${currentSV.email}</td>
        <td>${currentSV.tinhDTB()}</td>
        <td><button onclick="editSV('${currentSV.ma}')" class="btn btn-warning" >Sửa</button>
        <button onclick="deleteSV('${currentSV.ma}')" class="btn btn-danger" >Xóa</button></td>
        </tr>
        `;
    contentHTML = contentHTML + contentTr;
  }
  document.getElementById("tbodySinhVien").innerHTML = contentHTML;
}

function showInformation(sinhVien){
      document.getElementById("txtMaSV").value = sinhVien.ma
       document.getElementById("txtTenSV").value = sinhVien.ten
       document.getElementById("txtEmail").value = sinhVien.email
       document.getElementById("txtPass").value = sinhVien.matKhau
       document.getElementById("txtDiemToan").value = sinhVien.diemToan
       document.getElementById("txtDiemLy").value = sinhVien.diemLy
       document.getElementById("txtDiemHoa").value = sinhVien.diemHoa

}

function resetForm(){
  document.getElementById("formQLSV").reset()
  document.getElementById('txtMaSV').disabled = false

}