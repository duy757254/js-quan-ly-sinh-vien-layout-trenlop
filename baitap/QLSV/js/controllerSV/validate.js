function checkEmpty(value, idErr) {
    if (value.length == 0){
        document.getElementById(idErr).innerText= "Ô này không được để rỗng"
        return false
    }else{
        document.getElementById(idErr).innerText=""
        return true
    }
}
function checkIdSV(id,list,idErr){
    var index=list.findIndex(function(sv){
        return sv.ma == id})
        if (index == -1){
            document.getElementById(idErr).innerText=""
            return true
        }else{
            document.getElementById(idErr).innerText="Đã tồn tại"
            return false
        }
    }


function checkEmail(value, idErr) {
  const re =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    
  var isEmail = re.test(value);
  if (!isEmail) {
    document.getElementById(idErr).innerText = "Email không hợp lệ";
    return false;
  } else {
    document.getElementById(idErr).innerText = "";
    return true;
  }
}
